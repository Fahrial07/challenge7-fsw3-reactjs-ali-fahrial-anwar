import React from 'react';
import Navigasi  from '../components/navigasi';
import Main from '../components/main';
import FilterCars from '../components/filterCars';
import Footer from '../components/footer';

const Cars = () => {

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <header>
                        <Navigasi />
                        <Main />
                    </header>

                    <FilterCars />

                </div>
            </div>

            <Footer />

        </>
    );

}

export default Cars;