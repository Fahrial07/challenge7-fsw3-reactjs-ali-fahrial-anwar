import React from 'react';
import Navigation from '../components/navigasi';
import Main from '../components/main';
import Service from '../components/service';
import WhyUs from '../components/whyus';
import Testimonial from '../components/testimonial';
import Banner from '../components/banner';
import FAQ from '../components/faq';
import Footer from '../components/footer';


const Landing = () =>  {

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                <header>
                    <Navigation />
                    <Main />
                </header>
                    <Service />
                    <WhyUs />
                    <Testimonial />
                    <Banner />
                    <FAQ />
                </div>
            </div>
            <Footer />
        </>



    );

}

export default Landing;