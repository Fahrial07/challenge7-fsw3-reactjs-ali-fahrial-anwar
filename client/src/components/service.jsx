import React from 'react';



const  Service = () => {

    return (
        <section id="our-service">
                <div className="container d-flex justify-content-center">
                    <div className="our-service">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="row">
                                    <div className="col-md-6">
                                        <img src={process.env.PUBLIC_URL + 'img/img_service.png'} className="img-service" alt="" />
                                        {/* <span className="rounded-circle green ms-5 mt-4"></span>
                                        <span className="rounded-circle red"></span>
                                        <img className="img-blue ms-5" src="assets/img/blue-img.png" alt=" /">
                                        <span className="rounded-circle yellow"></span> */}
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="desc">
                                            <h5 className="desc-title">Best Car Rental for any kind of trip in (Lokasimu)!
                                            </h5>
                                            <p className="des-text">
                                                Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan
                                                harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                                                kualitas
                                                pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting,
                                                dll.
                                            </p>
                                            <div className="service-list-box">
                                                <div className="d-flex">
                                                    <img className="img-check-list" src={process.env.PUBLIC_URL + '/img/check-list.png'} alt="" />
                                                    <p className="our-list">Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                                                </div>
                                                <div className="d-flex mt-3">
                                                    <img className="img-check-list" src={process.env.PUBLIC_URL + 'img/check-list.png'} alt="" />
                                                    <p className="our-list">Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                                                </div>
                                                <div className="d-flex mt-3">
                                                    <img className="img-check-list" src={process.env.PUBLIC_URL + 'img/check-list.png'} alt="" />
                                                    <p className="our-list">Sewa Mobil Jangka Panjang Bulanan</p>
                                                </div>
                                                <div className="d-flex mt-3">
                                                    <img className="img-check-list" src={process.env.PUBLIC_URL + 'img/check-list.png'} alt="" />
                                                    <p className="our-list">Gratis Anter - Jemput Mobil di Bandara</p>
                                                </div>
                                                <div className="d-flex mt-3">
                                                    <img className="img-check-list" src={process.env.PUBLIC_URL + 'img/check-list.png'} alt="" />
                                                    <p className="our-list">Layanan Airport Transfer / Drop In Out</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    );

}

export default Service;