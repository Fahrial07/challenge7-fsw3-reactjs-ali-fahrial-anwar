import React from 'react';
import '../assets/css/style.css';

const Navigasi = () => {

    return(

                <nav className="navbar navbar-expand-md navbar-light">
                    <div className="container">
                        <a className="navbar-brand ms-4" href="/">
                            <div className="br"></div>
                        </a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                            aria-labelledby="offcanvasNavbarLabel">
                            <div className="offcanvas-header">
                                <h5 className="offcanvas-title" id="offcanvasNavbarLabel">BCR</h5>
                                <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"
                                    aria-label="Close"></button>
                            </div>
                            <div className="offcanvas-body">
                                <ul className="navbar-nav justify-content-end flex-grow-1">
                                    <li className="nav-item me-md-3">
                                        <a className="nav-link text-dark" aria-current="page" href="#our-service">Our
                                            Service</a>
                                    </li>
                                    <li className="nav-item me-md-3">
                                        <a className="nav-link text-dark" href="#why-us">Why Us</a>
                                    </li>
                                    <li className="nav-item me-md-3">
                                        <a className="nav-link text-dark" href="#testimonial">Testimonial</a>
                                    </li>
                                    <li className="nav-item me-md-3">
                                        <a className="nav-link text-dark" href="#faq">FAQ</a>
                                    </li>
                                    <button className="btn-nav" type="button">Register</button>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>



    );

}

export default Navigasi;