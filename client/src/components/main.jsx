import React from 'react';


const Main = () => {

    return (

        <div className="container-fluid slg">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="slog-space">
                                        <h5 className="slogan">Sewa & Rental Mobil Terbaik di Kawasan (Lokasimu)
                                        </h5>
                                        <p className="welcome-text">Selamat datang di Binar Car Rental. Kami menyediakan
                                            mobil
                                            kualitas
                                            terbaik
                                            dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil
                                            selama 24
                                            jam.</p>
                                        <button className="text-center text-white btn-mulai">
                                            <a href="/cars" className="text-decoration-none text-white">Mulai Sewa Mobil</a>
                                        </button>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <img className="bg-car" src={process.env.PUBLIC_URL + '/img/img_car.png'} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    );

}

export default Main;