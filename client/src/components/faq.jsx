import React from 'react';


const FAQ = () => {

    return (
        <section id="faq">
                <div className="container mt-5 pt-5 mb-5">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="row">
                                <div className="col-md-6">
                                    <h5 className="faq-title-top">Frequently Asked Question</h5>
                                    <p className="faq-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                </div>
                                <div className="col-md-6">

                                    <div className="accordion" id="accordionPanelsStayOpenExample">
                                        <div className="accordion-item mb-4">
                                            <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                                                <button className="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="false"
                                                    aria-controls="panelsStayOpen-collapseOne">
                                                    <h5 className="faq-title">Apa saja syarat yang dibutuhkan ?</h5>
                                                </button>
                                            </h2>
                                            <div id="panelsStayOpen-collapseOne" className="accordion-collapse collapse"
                                                aria-labelledby="panelsStayOpen-headingTwo">
                                                <div className="accordion-body">
                                                    <p className="faq-answer">
                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                        Assumenda
                                                        possimus
                                                        labore eius aperiam quidem, quod magni asperiores ipsa optio
                                                        doloribus?
                                                        Dolor rerum illo minima dignissimos quia repudiandae, facilis
                                                        iure
                                                        explicabo.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item mb-4" style={{border: 1 + "px solid #D4D4D4;"}}>
                                            <h2 className="accordion-header" id="panelsStayOpen-headingTwo">
                                                <button className="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false"
                                                    aria-controls="panelsStayOpen-collapseTwo">
                                                    <h5 className="faq-title">Berapa hari minimal sewa mobil lepas kunci ?
                                                    </h5>
                                                </button>
                                            </h2>
                                            <div id="panelsStayOpen-collapseTwo" className="accordion-collapse collapse"
                                                aria-labelledby="panelsStayOpen-headingTwo">
                                                <div className="accordion-body">
                                                    <p className="faq-answer">
                                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                                        Expedita
                                                        impedit
                                                        dolor non iste iusto commodi repellendus amet harum architecto
                                                        delectus
                                                        nulla, quasi tempore rerum odio pariatur, nisi fuga molestias
                                                        beatae.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item mb-4" style={{border: 1 + "px solid #D4D4D4;"}}>
                                            <h2 className="accordion-header" id="panelsStayOpen-headingThree">
                                                <button className="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false"
                                                    aria-controls="panelsStayOpen-collapseThree">
                                                    <h5 className="faq-title">Berapa hari sebelumnya sebaiknya booking sewa
                                                        mobil ?
                                                    </h5>
                                                </button>
                                            </h2>
                                            <div id="panelsStayOpen-collapseThree" className="accordion-collapse collapse"
                                                aria-labelledby="panelsStayOpen-headingThree">
                                                <div className="accordion-body">
                                                    <p className="faq-answer">
                                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                                        Pariatur,
                                                        rerum
                                                        expedita sed accusamus ipsam repellendus consequatur quo
                                                        assumenda
                                                        maiores
                                                        exercitationem velit molestiae nisi laborum impedit officiis
                                                        fugiat
                                                        voluptatem praesentium deserunt.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item mb-4" style={{border: 1 + "px solid #D4D4D4;"}}>
                                            <h2 className="accordion-header" id="panelsStayOpen-headingFour">
                                                <button className="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false"
                                                    aria-controls="panelsStayOpen-collapseFour">
                                                    <h5 className="faq-title">Apakah ada biaya antar-jemput ?</h5>
                                                </button>
                                            </h2>
                                            <div id="panelsStayOpen-collapseFour" className="accordion-collapse collapse"
                                                aria-labelledby="panelsStayOpen-headingFour">
                                                <div className="accordion-body">
                                                    <p className="faq-answer">
                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                        Veritatis est
                                                        ipsum
                                                        voluptatem, neque quisquam temporibus quibusdam debitis ad, quae
                                                        modi
                                                        numquam. Ex nesciunt temporibus asperiores, dignissimos quo
                                                        incidunt
                                                        odio
                                                        obcaecati!
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item" style={{border: 1 + "px solid #D4D4D4;"}}>
                                            <h2 className="accordion-header" id="panelsStayOpen-headingFive">
                                                <button className="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="false"
                                                    aria-controls="panelsStayOpen-collapseFive">
                                                    <h5 className="faq-title">Bagaimana jika terjadi kecelakaan ?</h5>
                                                </button>
                                            </h2>
                                            <div id="panelsStayOpen-collapseFive" className="accordion-collapse collapse"
                                                aria-labelledby="panelsStayOpen-headingFive">
                                                <div className="accordion-body">
                                                    <p className="faq-answer">
                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                        Excepturi
                                                        praesentium veniam officiis, iure perspiciatis voluptatibus
                                                        commodi
                                                        natus
                                                        debitis voluptate porro ipsa quae et, est cum atque modi facere
                                                        temporibus
                                                        cupiditate.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

    );

}

export default FAQ;