import React from 'react';


const Footer = () => {

    return (
        <footer>
        <div className="container">
            <div className="row mb-5 d-flex justify-content-evenly flex-wrap">
                <div className="col-lg-3">
                    <p className="address-bottom al">Jalan Suroyo No. 161 Mayangan Kota Probolinggo 672000</p>
                    <p className="address-bottom">binarcarrental@gmail.com</p>
                    <p className="address-bottom">081-233-334-808</p>
                </div>
                <div className="col-lg-3">
                    <a className="menu-bottom nav-link text-black text-decoration-none fw-bold" href="#our-service">Our
                        Service</a>
                    <a className="menu-bottom nav-link text-black text-decoration-none fw-bold" href="#why-us">Why Us</a>
                    <a className="menu-bottom nav-link text-black text-decoration-none fw-bold"
                        href="#testimonial">Testimonial</a>
                    <a className="menu-bottom nav-link text-black text-decoration-none fw-bold" href="##faq">FAQ</a>
                </div>
                <div className="col-lg-3">
                    <p className="connect text-black">Connect with us</p>
                    <a className="text-decoration-none me-2" href="true">
                        <img src={process.env.PUBLIC_URL + 'img/icon_facebook.svg'} alt="" />
                    </a>
                    <a className="text-decoration-none me-2" href="true">
                        <img src={process.env.PUBLIC_URL + 'img/icon_instagram.svg'} alt="" />
                    </a>
                    <a className="text-decoration-none me-2" href="true">
                        <img src={process.env.PUBLIC_URL + 'img/icon_twitter.svg'} alt="" />
                    </a>
                    <a className="text-decoration-none me-2" href="true">
                        <img src={process.env.PUBLIC_URL + 'img/icon_mail.svg'} alt="" />
                    </a>
                    <a className="text-decoration-none me-2" href="true">
                        <img src={process.env.PUBLIC_URL + 'img/icon_twitch.svg'} alt="" />
                    </a>
                </div>
                <div className="col-lg-3">
                    <p className="text-dark cr">Copyright Binar 2022</p>
                    <div className="logo-bottom"></div>
                </div>
            </div>
        </div>
    </footer>
    );

}

export default Footer;