import React from 'react';

const Banner = () => {

    return (
        <section id="banner">
                <div class="container bg-banner">
                    <div class="row d-flex flex-row justify-content-center ">
                        <div class="col-lg-12 text-white wd">
                            <h5 class="banner-title  text-center">Sewa Mobil di (Lokasimu) Sekarang</h5>
                            <p class="banner-desc text-center mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed
                                do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                            <button class="banner-btn mt-5">Mulai Sewa Mobil</button>
                        </div>
                    </div>
                </div>
            </section>
    );

}

export default Banner;
