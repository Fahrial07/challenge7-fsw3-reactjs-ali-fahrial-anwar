import React from 'react';

const Testimonial = () => {

    return (
        <section id="testimonial">
                <div className="container">
                    <div className="row mt-5">
                        <div className="col-lg-12">
                            <h5 className="text-center testimonial-title">Testimonial</h5>
                            <p className="text-center testiminial-desc">Berbagai riview positif dari pelanggan kami</p>

                            <div className="container mb-5 d-flex flex-row flex-wrap" style={{marginTop: 5 + "%" }}>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div id="carouselExampleIndicators" className="carousel slide"
                                            data-bs-ride="carousel">
                                            <ol className="carousel-indicators" style={{bottom: -70 + 'px;' }}>
                                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                                                    className="active" aria-current="true" aria-label="Slide 1"></li>
                                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                                                    aria-label="Slide 2"></li>
                                            </ol>
                                            <div className="carousel-inner">
                                                <div className="carousel-item active">
                                                    <div className="row">
                                                        <div className="single-box carausel-setting p-2 overflow-auto">
                                                            <div className="container">
                                                                <div className="row align-items-center">
                                                                    <div className="row">
                                                                        <div className="col-lg-12">
                                                                            <div className="row align-middle">
                                                                                <div className="col-lg-3">
                                                                                    <img src={process.env.PUBLIC_URL + 'img/img_photo.png'}
                                                                                        className="rounded-circle text-center" alt="" />
                                                                                </div>
                                                                                <div className="col-lg-9">
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{color: '#F9CC00;'}}></i>

                                                                                    <p className="text-box-testi">
                                                                                        “Lorem ipsum dolor sit amet,
                                                                                        consectetur
                                                                                        adipiscing
                                                                                        elit,
                                                                                        sed do eiusmod lorem ipsum dolor
                                                                                        sit
                                                                                        amet,
                                                                                        consectetur adipiscing elit, sed
                                                                                        do
                                                                                        eiusmod
                                                                                        lorem ipsum dolor sit
                                                                                        amet,consectetur
                                                                                        adipiscing
                                                                                        elit, sed do eiusmod”</p>
                                                                                    <h6 className="testi-name">John Dee 32,
                                                                                        Bromo</h6>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="carousel-item">
                                                    <div className="row">
                                                        <div
                                                            className="single-box carausel-setting p-2 overflow-auto mx-auto my-auto">
                                                            <div className="container">
                                                                <div className="row align-items-center">
                                                                    <div className="row">
                                                                        <div className="col-lg-12">
                                                                            <div className="row align-middle">
                                                                                <div className="col-lg-3">
                                                                                    <img src={process.env.PUBLIC_URL + 'img/img_photo.png'}
                                                                                        className="rounded-circle text-center" alt="" />
                                                                                </div>
                                                                                <div className="col-lg-9">
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{ color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{ color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{ color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{ color: '#F9CC00;'}}></i>
                                                                                    <i className="bi bi-star-fill"
                                                                                        style={{ color: '#F9CC00;'}}></i>

                                                                                    <p className="text-box-testi">
                                                                                        “Lorem ipsum dolor sit amet,
                                                                                        consectetur
                                                                                        adipiscing
                                                                                        elit,
                                                                                        sed do eiusmod lorem ipsum dolor
                                                                                        sit
                                                                                        amet,
                                                                                        consectetur adipiscing elit, sed
                                                                                        do
                                                                                        eiusmod
                                                                                        lorem ipsum dolor sit
                                                                                        amet,consectetur
                                                                                        adipiscing
                                                                                        elit, sed do eiusmod”</p>
                                                                                    <h6 className="testi-name">John Dee 32,
                                                                                        Bromo</h6>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    );

}

export default Testimonial;