import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCars, carsSelector } from '../features/carsSlice';


function FilterCars(){

    const [driver, setDriver] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [ passenger, setPassenger ] = useState('');


    const dispatch = useDispatch();
    const cars = useSelector(carsSelector.selectAll);

    // let filterCars;

    useEffect(() => {
       dispatch(getCars());
    }, [dispatch]);

        const getData = async (e) => {
            e.preventDefault();
            // console.log(driver, date, time, passenger);
        }
        let filterCars = cars.filter((car) => car.capacity == passenger && car.time == time || car.date == date && car.driver == driver);



    return (

            <div className="row justify-content-center mt-2">
                <div className="search-filter col-lg-8" id="search-filter">

                    <div className="row align-items-center justify-content-center mt-3 mb-3">
                       <form onSubmit={getData} className="row align-items-center justify-content-center mt-3 mb-3">
                            <div className="col-lg-3 my-1" id="sm">
                            <label htmlFor="driverType">Tipe Driver</label><br />
                            <select className="form-select" name="driverType" id="driverType" onChange={(e) => setDriver(e.target.value)}>
                                <option value="" disabled hidden>Pilih Tipe Driver</option>
                                <option value="Dengan Sopir">Dengan Sopir</option>
                                <option value="Keyless Entry">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                        <div className="col-lg-2 my-1" id="sm">
                            <label htmlFor="passengerCapacity">Jumlah Penumpang</label><br />
                            <input className="form-control" required type="text" onChange={(e) => setPassenger(e.target.value)}  name="passengerCapacity"
                                id="passengerCapacity" />
                        </div>
                        <div className="col-lg-2 my-1" id="sm">
                            <label htmlFor="Time">Waktu Ambil</label><br />
                            <select className="form-select" required name="Time" id="Time" onChange={(e) => setTime(e.target.value)}>
                                <option value="" disabled hidden>Pilih Waktu</option>
                                <option value="8">08.00 WIB</option>
                                <option value="9">09.00 WIB</option>
                                <option value="10">10.00 WIB</option>
                                <option value="11">11.00 WIB</option>
                                <option value="12">12.00 WIB</option>
                            </select>
                        </div>
                        <div className="col-lg-3 my-1" id="sm">
                            <label htmlFor="dateAvailable">Pilih Tanggal</label><br />
                            <input className="form-control" type="date" onChange={(e) => setDate(e.target.value)} required name="dateAvailable" id="dateAvailable" />
                        </div>
                        <div className="col-lg-2 my-1 d-flex flex-column justify-content-center">
                            <p></p>
                            <button className="btn btn-success btn-sm" type="submit" id="btn-search">Cari Mobil</button>
                        </div>
                       </form>

                    </div>

                </div>


                <div className="container-fluid d-flex flex-row justify-content-center flex-wrap mt-5" id="result-container">

                    { filterCars.map((car, index) => (
                        <div key={index} className="box me-3 mb-3" >
                            <>
                            <div className="frame">
                                <img src={car.image} alt="" className="img-r" style={{ width: 200+'px' }} />
                                    <h5 className="text-center">{car.name}</h5>
                                    <h6 className="fw-bold"><i className="bi bi-cash"></i>  Rp. {car.price}  / Hari</h6>
                                    <p className="fw-bold"><i className="bi bi-people-fill"></i>&nbsp; {car.capacity}</p>
                                    <p><i className="bi bi-gear-fill"></i>&nbsp; {car.driver} </p>
                                    <p><i className="bi bi-calendar-check-fill"></i>&nbsp; {car.date}</p>
                                    <small>{car.description}</small>
                            </div>
                            </>
                            <button className="pilih fw-bold text-white mt-4">Pilih Mobil</button>
                        </div>


                    ))}

                </div>


            </div>





    );

}

export default FilterCars;