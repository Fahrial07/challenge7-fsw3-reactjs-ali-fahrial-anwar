import React from 'react';

const WhyUs = () => {

    return (

            <section className="why" id="why-us">
                <div className="container-fluid">
                    <div className="row">
                        <h5 className="text-left why-title">Why Us ?</h5>
                        <p className="text-left why-desc">Mengapa harus pilih Binar Car Rental ?</p>
                        <div className="col-lg-12 d-flex flex-row justify-content-evenly flex-wrap">
                            <div className="card mb-3">
                                <img className="icon" src={process.env.PUBLIC_URL + '/img/icon_complete.png'} alt=""/>
                                <h5 className="icon-title">Mobil Lengkap</h5>
                                <p className="icon-desc">Tersedia banyak pilihan mobil, kondisi masih baru,
                                    bersih dan
                                    terawat
                                </p>
                            </div>

                            <div className="card mb-3 c2">
                                <img className="icon" src={process.env.PUBLIC_URL + '/img/icon_price.png'} alt="" />
                                <h5 className="icon-title">Harga Murah</h5>
                                <p className="icon-desc">Harga murah dan bersaing, bisa bandingkan harga kami
                                    dengan
                                    rental
                                    mobil lain</p>
                            </div>
                            <div className="card mb-3 c3">
                                <img className="icon" src={process.env.PUBLIC_URL + 'img/icon_24hrs.png'} alt="" />
                                <h5 className="icon-title">Layanan 24 Jam</h5>
                                <p className="icon-desc">Siap melayani kebutuhan Anda selama 24 jam nonstop.
                                    Kami juga
                                    tersedia
                                    di akhir
                                    minggu
                                </p>
                            </div>

                            <div className="card mb-3 c4">
                                <img className="icon" src={process.env.PUBLIC_URL + 'img/icon_professional.png'} alt="" />
                                <h5 className="icon-title">Sopir Professional</h5>
                                <p className="icon-desc">Sopir yang profesional, berpengalaman, jujur, ramah dan
                                    selalu
                                    tepat
                                    waktu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    );

}

export default WhyUs;