import React from 'react';
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';

    export const getCars = createAsyncThunk("cars/getCars", async() => {
        const response = await axios.get('http://localhost:8000/api/v1/search');

        return response.data.data;

    });

    const carsEntity = createEntityAdapter({
        selectId: (cars) => cars.id
    });

    const carsSlice = createSlice({
            name: "cars",
            initialState: carsEntity.getInitialState(),
            extraReducers: {
                //get data dari api dan set ke initial state
                [getCars.fulfilled]: (state, action) => {
                    carsEntity.setAll(state, action.payload);
                }
            }
        });

    export const carsSelector = carsEntity.getSelectors(state => state.cars);
    export default carsSlice.reducer;

